/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrimish;

/**
 *
 * @author wqu
 */
public class Cards {
    private String name;
    private int value;
    
    public Cards(String initName, int initValue){
        name = initName;
        value = initValue;
    }
    
    public String getName(){
        return name;
    }
    
    public int getValue(){
        return value;
    }
}
