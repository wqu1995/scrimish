/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrimish;

import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author wqu
 */
public class Computer {
    private final Stack<Cards>[] deck;
    private Scanner input = new Scanner(System.in);
    private Random rn = new Random();
    
    public Computer(){
        deck = new Stack[5];
        
        for(int i = 0; i <5 ; i++){
            deck[i] = new Stack();
        }
    }
    
    public void setUp(){
        pushCard("Crown", 0);
        int counter = 0;
        int daggerCounter = 0;
        int swordCounter = 0;
        int starCounter = 0;
        int axeCounter = 0;
        int halberdCounter = 0;
        int longswordCounter = 0;
        int archerCounter = 0;
        int shieldCounter = 0;
        
        while(counter <24){
            int x = rn.nextInt(8)+1; 
            boolean succeed = false;
            
            switch (x){
                case 1:
                    if(daggerCounter<5){
                        pushCard("Dagger",x);
                        daggerCounter++;
                        succeed = true;
                        break;
                    }
                    else
                        break;
                case 2:
                    if(swordCounter<5){
                        pushCard("Sword",x);
                        swordCounter++;
                        succeed = true;
                        break;
                    }
                    else
                        break;
                case 3:
                    if(starCounter<3){
                        pushCard("Morning Star",x);
                        starCounter++;
                        succeed = true;
                        break;
                    }
                    else
                        break;
                case 4:
                    if(axeCounter<3){
                        pushCard("War Axe",x);
                        axeCounter++;
                        succeed = true;
                        break;
                    }
                    else
                        break;
                case 5:
                    if(halberdCounter<2){
                        pushCard("Halberd",x);
                        halberdCounter++;
                        succeed = true;
                        break;
                    }
                    else
                        break;
                case 6:
                    if(longswordCounter<2){
                        pushCard("Longsword",x);
                        longswordCounter++;
                        succeed = true;
                        break;
                    }
                    else
                        break;
                case 7:
                    if(archerCounter<2){
                        pushCard("Archer",x);
                        archerCounter++;
                        succeed = true;
                        break;
                    }
                    else
                        break;
                case 8:
                    if(shieldCounter<2){
                        pushCard("Shield",x);
                        shieldCounter++;
                        succeed = true;
                        break;
                    }
                    else
                        break;
            }
            if(succeed){
                counter++;
                
            }
        }
    }
    public Stack<Cards>[] getDeck(){
        return deck;
    }
    private void pushCard(String name, int value){
        boolean trigger = true;
        
        while(trigger){
            int pileNumber = rn.nextInt(5);
            
            if(deck[pileNumber].size()<5){
                deck[pileNumber].push(new Cards(name, value));
                trigger= false;
            }
        }
    }
}
