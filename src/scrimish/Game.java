/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrimish;

import java.util.Stack;

/**
 *
 * @author wqu
 */
public class Game {
    private Player player;
    private Computer computer;
    public Game(){
        player = new Player();
        computer = new Computer();
    }
    
    public void start(){
       // player.setUp();
        computer.setUp();
        
        Stack<Cards>[] test = computer.getDeck();
        
        for(int i = 0; i < test.length; i++){
            while(!test[i].empty()){
                System.out.print(test[i].pop().getName()+ " ");
            }
            System.out.println();
        }
        for(int i = 0; i< test.length; i++){
            System.out.println(test[i].size());
        }
    }
}
