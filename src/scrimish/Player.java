/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrimish;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author wqu
 */
public class Player {
    private final Stack<Cards>[] deck;
    private Scanner input = new Scanner(System.in);
    
    public Player(){
        deck = new Stack[5];
        
        for(int i = 0; i <5 ; i++){
            deck[i] = new Stack();
        }
    }
    
    public void setUp(){
        int counter = 0;
        int daggerCounter = 0;
        int swordCounter = 0;
        int starCounter = 0;
        int axeCounter = 0;
        int halberdCounter = 0;
        int longswordCounter = 0;
        int archerCounter = 0;
        int shieldCounter = 0;
        System.out.println("Please place yoru crown first.");
        pushCard("Crown", 0);
        
        while(counter< 24){
            boolean succeed = false;
            if(daggerCounter<5){
                System.out.print("Enter 1 to select Dagger\n");
            }
            if(swordCounter<5){
                System.out.print("Enter 2 to select Sword \n");
            }
            if(starCounter<3){
                System.out.print("Enter 3 to select Morning Star \n");
            }
            if(axeCounter<3){
                System.out.print("Enter 4 to select War Axe \n");
            }
            if(halberdCounter<2){
                System.out.print("Enter 5 to select Halberd \n");
            }
            if(longswordCounter<2){
                System.out.print("Enter 6 to select Longsword \n");
            }
            if(archerCounter<2){
                System.out.print("Enter A to select Archer \n");
            }
            if(shieldCounter<2){
                System.out.print("Enter S to select Shield \n");
            }
            switch(input.next().charAt(0)){
                case '1':
                    if(daggerCounter<5){
                        pushCard("Dagger",1);
                        daggerCounter++;
                        succeed = true;
                        break;
                    }
                    else{
                        System.out.println("You have exceed the amount of cards in this type. Please select another vaild card!");
                        break;
                    }
                case '2':
                    if(swordCounter<5){
                        pushCard("Sword",2);
                        swordCounter++;
                        succeed = true;
                        break;
                    }
                    else{
                        System.out.println("You have exceed the amount of cards in this type. Please select another vaild card!");
                        break;
                    }
                case '3':
                        if(starCounter<3){
                        pushCard("Morning Star",3);
                        starCounter++;
                        succeed = true;
                        break;
                    }
                    else{
                        System.out.println("You have exceed the amount of cards in this type. Please select another vaild card!");
                        break;
                    }
                case '4':
                        if(axeCounter<3){
                        pushCard("War Axe",4);
                        axeCounter++;
                        succeed = true;
                        break;
                    }
                    else{
                        System.out.println("You have exceed the amount of cards in this type. Please select another vaild card!");
                        break;
                    }
                case '5':
                        if(halberdCounter<2){
                        pushCard("Halberd",5);
                        halberdCounter++;
                        succeed = true;
                        break;
                    }
                    else{
                        System.out.println("You have exceed the amount of cards in this type. Please select another vaild card!");
                        break;
                    }
                case '6':
                    if(longswordCounter<2){
                        pushCard("Longsword",6);
                        longswordCounter++;
                        succeed = true;
                        break;
                    }
                    else{
                        System.out.println("You have exceed the amount of cards in this type. Please select another vaild card!");
                        break;
                    }
                case 'A':
                    if(archerCounter<2){
                        pushCard("Archer",7);
                        archerCounter++;
                        succeed = true;
                        break;
                    }
                    else{
                        System.out.println("You have exceed the amount of cards in this type. Please select another vaild card!");
                        break;
                    }
                case 'S':
                    if(shieldCounter<2){
                        pushCard("Shield",8);
                        shieldCounter++;
                        succeed = true;
                        break;
                    }
                    else{
                        System.out.println("You have exceed the amount of cards in this type. Please select another vaild card!");
                        break;
                    }
            }
            if(succeed)
                counter++;
        }
    }
    
    public Stack<Cards>[] getDeck(){
        return deck;
    }
    
    private void pushCard(String name, int value){
        boolean trigger = true;
        
        while(trigger){
            System.out.print("Enter the number of pile: ");
            int x = input.nextInt()-1;
            if(deck[x].size()<5){
                deck[x].push(new Cards(name, value));
                trigger = false;
            }
            else{
                System.out.println("Pile is already full. Please select another pile");
            }
        }
    }
    
        
}
